const {
  replyDrap,
  replyMuf,
  replySolar101,
  replyDipoleLengths,
  replyLastFlare,
  replyContests,
} = require("./methods");

const commands = [
  {
    name: "drap",
    aliases: ["d-rap", "blackout"],
    description: "Replies with latest noaa drap solar radio propagation map.",
    function: replyDrap,
  },
  {
    name: "solar101",
    aliases: ["solar", "solar-101"],
    description: "Replies with latest hamsql solar 101 image.",
    function: replySolar101,
  },
  {
    name: "muf",
    aliases: ["mufcalc", "muf-calc"],
    description: "Replies with latest MUF image from kc2g",
    function: replyMuf,
  },
  {
    name: "lastflare",
    aliases: ["flare", "xray"],
    description: "Replies with latest flare data from NOAA",
    function: replyLastFlare,
  },
  {
    name: "contests",
    aliases: ["contest", "hamcontest"],
    description: "Replies with latest contest data from contestcalendar.com",
    function: replyContests,
    ephemeral: true,
  },
  {
    name: "dipolecalc",
    aliases: ["dipole", "dipole-calc", "wavelength"],
    description: "Replies with dipole length calculations given a frequency",
    options: [
      {
        type: 10,
        name: "mhz",
        description: "Desired resonant frequency in MHz",
        required: true,
      },
    ],
    function: replyDipoleLengths,
  },
  {
    name: "commands",
    aliases: ["help"],
    description: "Replies with the commands.",
  },
];

exports.commands = commands;

require("dotenv").config();
const {
  Client,
  GatewayIntentBits,
  REST,
  Routes,
  EmbedBuilder,
} = require("discord.js");
const { replyCommandList } = require("./methods/replyCommandList");
const { commands } = require("./commands");
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

const rest = new REST({ version: "10" }).setToken(process.env.TOKEN);

const registeredCommands = [];

(async () => {
  try {
    console.log("Started refreshing application (/) commands.");

    const postCommands = commands.map((command) => {
      const { name, description, options, aliases } = command;
      return { name, description, options, aliases };
    });

    await rest
      .put(Routes.applicationCommands(process.env.CLIENT_ID), {
        body: postCommands,
      })
      .then((res) => {
        console.log(res);
        registeredCommands.push(...res);
      });

    console.log("Successfully reloaded application (/) commands.");
  } catch (error) {
    console.error(error);
  }
})();

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("interactionCreate", async (interaction) => {
  try {
    if (!interaction.isChatInputCommand()) return;

    // due to circular import, this has to be handled separately
    if (interaction.commandName === "commands") {
      await interaction.deferReply({ ephemeral: true });
      await replyCommandList(interaction, registeredCommands);
      return;
    }

    commands.forEach(async (command) => {
      if (interaction.commandName === command.name) {
        const { ephemeral = false } = command;
        await interaction.deferReply({ ephemeral });
        await command.function(interaction);
      }
    });
  } catch (e) {
    console.error(e);
    interaction
      .reply({
        content:
          "Sorry, something went wrong. Try again in a bit, or check the console logs for more.",
      })
      // I'm really disappointed if we reach this part...
      // best to cut my losses and hope the next reincarnation doesn't
      .catch((e) => {
        throw e;
      });
  }
});

console.log(
  commands.map((command) => `${command.name}: ${command.description}\n`)
);

client.login(process.env.TOKEN).catch((e) => {
  throw e;
});

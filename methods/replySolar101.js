const axios = require("axios");
const { interact } = require("./interact");

function replySolar101(interaction) {
  axios
    .get("https://www.hamqsl.com/solar101pic.php", {
      responseType: "arraybuffer",
    })
    .then(async (data) => {
      const content = `Current HamSQL Solar 101 Image as of ${new Date().toUTCString()} / ${new Date().toLocaleString(
        "en-AU",
        {
          timeZone: "Australia/Melbourne",
        }
      )} in Melbourne, Australia`;
      const files = [data.data];
      interact(interaction, content, files);
    });
}
exports.replySolar101 = replySolar101;

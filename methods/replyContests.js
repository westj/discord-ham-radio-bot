const axios = require("axios");
const ical = require("node-ical");
const discord = require("discord.js");
const { interact } = require("./interact");

function replyContests(interaction) {
  axios
    .get("https://www.contestcalendar.com/weeklycontcustom.ics", {
      responseType: "text",
    })
    .then(async (data) => {
      const events = ical.sync.parseICS(data.data);

      const eventStrings = Object.keys(events).map((eventKey) => {
        if (eventKey === "vcalendar") return;
        const { summary, start, end, url } = events[eventKey];
        const startDate = new Date(start);
        const endDate = new Date(end);

        // if startDate is in the past, skip
        if (startDate < new Date()) return;

        const startString = startDate.toLocaleString("en-AU", {
          timeZone: "UTC",
        });
        const endString = endDate.toLocaleString("en-AU", { timeZone: "UTC" });

        return `.\n${summary}\t${startString} - ${endString}\n${url}\n`;
      });

      const title = `Contest list for the next week:`;

      // create an array that contains 5 eventstrings each
      const eventStringArrays = [];
      let i = 0;
      while (i < eventStrings.length) {
        eventStringArrays.push(eventStrings.slice(i, i + 15).join(""));
        i += 15;
      }

      const content = [title, ...eventStringArrays];

      // const content = `beep boop stub command`;
      interact(interaction, content);
    });
}
exports.replyContests = replyContests;

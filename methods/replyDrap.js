const axios = require("axios");
const { interact } = require("./interact");

function replyDrap(interaction) {
  axios
    .get(
      "https://services.swpc.noaa.gov/images/animations/d-rap/global/d-rap/latest.png",
      { responseType: "arraybuffer" }
    )
    .then(async (data) => {
      const content = `Current NOAA D-RAP Image as of ${new Date().toUTCString()} / ${new Date().toLocaleString(
        "en-AU",
        {
          timeZone: "Australia/Melbourne",
        }
      )} in Melbourne, Australia`;
      const files = [data.data];
      interact(interaction, content, files);
    });
}
exports.replyDrap = replyDrap;

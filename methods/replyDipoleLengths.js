const { interact } = require("./interact");

async function replyDipoleLengths(interaction) {
  const mhz = interaction.options.getNumber("mhz");

  const hw = parseFloat(`${299.792458 / mhz / 4}`).toFixed(4);
  const qw = parseFloat(`${299.792458 / mhz / 8}`).toFixed(4);
  const ofc = [
    parseFloat(`${(299.792458 / mhz / 2) * 0.66}`).toFixed(4),
    parseFloat(`${(299.792458 / mhz / 2) * 0.33}`).toFixed(4),
  ];

  content = `For ${mhz}MHz...
    On a symmetric (single-band) half-wave dipole, your elements should be **${hw} metres** long each
    On a symmetric (single-band) quarter-wave dipole, your elements should be **${qw} metres** long each
    On an off-centre-fed (multi-band) dipole, your elements should be **${ofc[0]}** and **${ofc[1]} metres** long with a 4:1 balun`;

  interact(interaction, content);
}
exports.replyDipoleLengths = replyDipoleLengths;

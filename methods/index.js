const { replyDipoleLengths } = require("./replyDipoleLengths");
const { replyDrap } = require("./replyDrap");
const { replyLastFlare } = require("./replyLastFlare");
const { replyMuf } = require("./replyMufd");
const { replySolar101 } = require("./replySolar101");
const { replyContests } = require("./replyContests");

module.exports = {
  replyDipoleLengths,
  replyDrap,
  replyLastFlare,
  replyMuf,
  replySolar101,
  replyContests,
};

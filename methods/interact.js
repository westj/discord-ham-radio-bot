async function interact(interaction, content, files) {
  const { ephemeral } = interaction;
  // check if content is a string or array
  if (typeof content === "string") {
    await interaction
      .followUp({
        content,
        files,
        ephemeral,
      })
      .catch((e) => {
        console.error(e);
      });
  } else if (Array.isArray(content)) {
    content.forEach(async (content) => {
      // skip if content is empty
      if (content === "") return;
      await interaction
        .followUp({
          content,
          ephemeral,
        })
        .catch((e) => {
          console.error(e);
        });
    });
  }
}

exports.interact = interact;

const axios = require("axios");
const sharp = require("sharp");
const { interact } = require("./interact");

function replyMuf(interaction) {
  axios
    .get("https://prop.kc2g.com/renders/current/mufd-normal-now.svg", {
      responseType: "arraybuffer",
    })
    .then(async (data) => {
      sharp(data.data)
        .png()
        .toBuffer(async (e, data) => {
          const content = `Current MUF Image as of ${new Date().toUTCString()} / ${new Date().toLocaleString(
            "en-AU",
            {
              timeZone: "Australia/Melbourne",
            }
          )} in Melbourne, Australia`;
          const files = [data];
          interact(interaction, content, files);
        });
    });
}
exports.replyMuf = replyMuf;

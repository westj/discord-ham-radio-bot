const { commands } = require("../commands");
const { interact } = require("./interact");

async function replyCommandList(interaction, registeredCommands) {
  const content =
    "Bot Commands:\n" +
    commands
      .map((command) => {
        const registeredCommand = registeredCommands.find(
          (registeredCommand) => registeredCommand.name === command.name
        );

        return `**</${registeredCommand.name}:${registeredCommand.id}>**: ${
          command.description
        }  _[${
          command.ephemeral ? "Visible to **you**" : "Visible to **everyone**"
        }]_\n`;
      })
      .join("")
      .toString();
  await interact(interaction, content);
}
exports.replyCommandList = replyCommandList;

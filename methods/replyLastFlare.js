const axios = require("axios");
const { interact } = require("./interact");

function replyLastFlare(interaction) {
  axios
    .get(
      "https://services.swpc.noaa.gov/json/goes/primary/xray-flares-latest.json"
    )
    .then(async (data) => {
      const {
        time_tag,
        current_class,
        begin_time,
        begin_class,
        max_time,
        max_class,
        end_time,
        end_class,
      } = data.data[0];

      function reqn(flare) {
        const needsn = ["A", "M", "X"];

        output = "";
        needsn.forEach((e) => {
          if (flare === "Unk" || flare === undefined || flare === null) return;
          if (flare[0] == e) output = "n";
        });

        return output;
      }

      let content;

      const no_data_msg =
        max_time === "Unk" &&
        end_time === "Unk" &&
        begin_time === "Unk" &&
        current_class === null
          ? "No data available."
          : "";

      const {
        max_time_msg,
        end_time_msg,
        begin_time_msg,
        current_class_msg,
        radio_blackout_msg,
      } = {
        max_time_msg:
          max_time === "Unk"
            ? ""
            : `\nIt peaked at **${new Date(
                max_time
              ).toUTCString()}** as a${reqn(
                max_class
              )} **${max_class}** class.`,
        end_time_msg:
          end_time === "Unk"
            ? ""
            : `\nIt ended at **${new Date(end_time).toUTCString()}** as a${reqn(
                end_class
              )} **${end_class}** class.`,
        begin_time_msg:
          begin_time === "Unk"
            ? ""
            : `\nIt started at **${new Date(
                begin_time
              ).toUTCString()}** as a${reqn(
                begin_class
              )} **${begin_class}** class.`,
        current_class_msg:
          current_class === "Unk"
            ? ""
            : `\nCurrent X-Ray average is **${current_class}**`,
        radio_blackout_msg:
          current_class[0] === "X" || current_class[0] === "M"
            ? `\n***Radio Blackout is likely in effect in daytime areas. Check /drap for more information.***`
            : "",
      };

      content = `Last flare data:\nData current as of ${new Date(
        time_tag
      ).toUTCString()}.${begin_time_msg}${max_time_msg}${end_time_msg}${current_class_msg}${radio_blackout_msg}${no_data_msg}`;

      interact(interaction, content);
    });
}
exports.replyLastFlare = replyLastFlare;
